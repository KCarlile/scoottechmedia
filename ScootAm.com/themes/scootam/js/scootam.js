jQuery(function($){
	$(document).ready(function(){
		$("#comment-form").hide();

		$("#add-comment").click(function() {
				$("#comments #comment-form").show("slow");
				$(this).hide();
			} // end click function
		);

		$(".comment-text .links .comment-reply a").hide();
		$("#post-content .links .comment-add a").hide();
		$("li.comment-add").hide();
		$("#switch_edit-field-promo-description-und-0-value").hide();
		$("#switch_edit-comment-body-und-0-value").hide();
	}); // END doc ready
}); // END function