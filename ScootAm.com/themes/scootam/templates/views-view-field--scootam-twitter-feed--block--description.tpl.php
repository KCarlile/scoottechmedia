<?php
 /**
  * This template is used to print a single field in a view. It is not
  * actually used in default Views, as this is registered as a theme
  * function which has better performance. For single overrides, the
  * template is perfectly okay.
  *
  * Variables available:
  * - $view: The view object
  * - $field: The field handler object that can process the input
  * - $row: The raw SQL result that can be used
  * - $output: The processed output that will normally be used.
  *
  * When fetching output from the $row, this construct should be used:
  * $data = $row->{$field->field_alias}
  *
  * The above will guarantee that you'll always get the correct data,
  * regardless of any changes in the aliasing that might happen if
  * the view is modified.
  */
?>
<?php
  // strip "ScootAmTweets: " from the beginning of all tweets
  $pattern = '/^ScootAmTweets: /';
  $output = preg_replace($pattern, '', $output);

  // turn links into actual links
  // -- easy ones first
  $pattern ='/(http\:\/\/\S+)/';
  $replace = '<a href="$1" target="_blank">$1</a>';
  $output = preg_replace($pattern, $replace, $output);

  // -- links missing http://, but starting with www.
  $pattern ='/(www\.\S+)/';
  $replace = '<a href="http://$1" target="_blank">$1</a>';
  $output = preg_replace($pattern, $replace, $output);

  // turn @UserName references into links
  $pattern ='/@(\w+)/';
  $replace = '<a href="http://twitter.com/$1" target="_blank">@$1</a>';
  $output = preg_replace($pattern, $replace, $output);

  // turn hashtags into links
  $pattern ='/#(\w+)/';
  $replace = '<a href="https://twitter.com/#!/search?q=%23$1" target="_blank">#$1</a>';
  $output = preg_replace($pattern, $replace, $output);

  print $output;
?>