<!DOCTYPE html>
<head>
<?php print $head; ?>
<title><?php print $head_title; ?></title>
<?php
  if(drupal_is_front_page())
  {
?>
  <meta name="title" content="ScootAm - Promoting Freestyle Scooter Amateurs" />
  <meta name="description" content="ScootAm is your resource for videos, photos, and announcements from freestyle scooter amateurs and beginners. You can even submit your own scooter promo content and it will be published for the world to see!" />
<?php
  }
?>
<?php print $styles; ?>
<?php print $scripts; ?>
<!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<link rel="image_src" href="http://www.scootam.com/sites/all/themes/scootam/images/scootam-avatar.png" />
</head>
<body class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>
