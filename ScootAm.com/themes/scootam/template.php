<?php

/*
 * implementation of theme_preprocess_node(&$vars)
 */
function scootam_preprocess_node(&$vars) {
  $node = $vars['node'];
	$vars['submitted'] = format_date($node->created);

  if($node->type == 'promo') {
    if(isset($vars['content']['links']['node']['#links']['node-readmore'])) {
      $vars['content']['links']['node']['#links']['node-readmore']['title'] =
        str_replace("Read more", "View and Comment",
          $vars['content']['links']['node']['#links']['node-readmore']['title']);
    } // end if test
    
    if($node->field_promo_featured['und'][0]['value'] == 1) {
      $vars['classes_array'][] = 'featured-promo';
    } // end if test
  } // end if test
} // end function scootam_preprocess_node

function scootam_preprocess_views_view(&$vars) {
	if (isset($vars['view']->name)) {
  	$function = 'scootam_preprocess_views_view__'.$vars['view']->name;
		if (function_exists($function)) {
			$function($vars);
		} // end if test
	} // end if test
} // end function scootam_preprocess_views_view

function scootam_preprocess_views_view__rider_listing(&$vars) {
	$view = $vars['view'];
	// Create a variable that divides number of results in half and add one.
	$vars['half'] = ceil((count($view->result) / 2) + 1);
} // end function scootam_preprocess_views_view__rider_listing