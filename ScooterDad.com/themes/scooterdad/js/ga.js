  var $ = jQuery;

  $(document).ready(function() {
    if(jQuery.isFunction(_gaq.push)) {
      /* add onclick events for IndustryLinks page entries */
      $('a.IndustryLink').click(function() {
        _gaq.push(['_trackEvent', 'Content', 'IndustryLinks', $(this).attr('id')]);
      });
      
      /* add onclick events for sidebar content */
      $('a.SocialLink').click(function() {
        _gaq.push(['_trackEvent', 'Sidebar', 'SocialLink', $(this).attr('id')]);
      });
      
      /* add onclick events for Header Featured ads entries */
      $('a.AdHeaderFeatured').click(function() {
        _gaq.push(['_trackEvent', 'Ads', 'HeaderFeatured', $(this).attr('id')]);
      });
      
      /* add onclick events for Header ads entries */
      $('a.AdHeader').click(function() {
        _gaq.push(['_trackEvent', 'Ads', 'Header', $(this).attr('id')]);
      });
      
      /* add onclick events for Footer ads entries */
      $('a.AdFooter').click(function() {
        _gaq.push(['_trackEvent', 'Ads', 'Footer', $(this).attr('id')]);
      });
      
      /* add onclick events for Sidebar Featured ads entries */
      $('a.AdSidebarFeatured').click(function() {
        _gaq.push(['_trackEvent', 'Ads', 'SidebarFeatured', $(this).attr('id')]);
      });
      
      /* add onclick events for Sidebar ads entries */
      $('a.AdSidebar').click(function() {
        _gaq.push(['_trackEvent', 'Ads', 'Sidebar', $(this).attr('id')]);
      });
    }
  });