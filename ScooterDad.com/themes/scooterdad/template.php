<?php // $Id$


/**
 * Formats a product's model number.
 *
 * @ingroup themeable
 */
function scooterdad_uc_product_model($variables) {
  $output = '<div class="product-info model">' .
    '<span class="uc-model-label">' . t('SKU') . ':</span> ' .
    '<span class="uc-model">' . implode(", ", array('@sku' => $variables['model'])) . '</span>' .
    '</div>';

  return $output;
}


/**
 * Formats a product's length, width, and height.
 *
 * @ingroup themeable
 */
function scooterdad_uc_product_dimensions($variables) {
  $length = $variables['length'];
  $width = $variables['width'];
  $height = $variables['height'];
  $units = $variables['units'];
  
  $output = '';
  $dimensions = array();
  
  if($length != '0') {
    $dimensions['long'] = $length;
  }
  
  if($width != '0') {
    $dimensions['wide'] = $width;
  }
  
  if($height != '0') {
    $dimensions['high'] = $height;
  }
  
  foreach($dimensions as $key => $dim) {
    $dimensions[$key] = str_replace('in', 'inches', uc_length_format($dim, $units)) . ' ' . $key; 
  }
  
  $dimStr = implode(" x ", $dimensions);
  
  if(strlen($dimStr) > 0) {
    $output = '<div class="product-info dimensions">' .
      '<span class="uc-dimensions-label">' . t('Dimensions') .
      ':</span> <span class="uc-dimensions">' . $dimStr . '</span></div>';
  }
  
  return $output;
}

/**
 * Formats a product's price.
 *
 * @param $variables
 *   A render element where: 'element' => array(), and has the following keys:
 *   - #value: Price to be formated.
 *   - #attributes: (optional) Array of attributes to apply to enclosing DIV.
 *   - #title: (optional) Title to be used as label.
 *
 * @ingroup themeable
 */
function scooterdad_uc_product_price($variables) {
  $element = $variables['element'];
  $price = $element['#value'];
  $attributes = isset($element['#attributes']) ? $element['#attributes'] : array();
  $label = isset($element['#title']) ? $element['#title'] : '';

  if (isset($attributes['class'])) {
    array_unshift($attributes['class'], 'product-info');
  }
  else {
    $attributes['class'] = array('product-info');
  }

  $output = '<div ' . drupal_attributes($attributes) . '>';
  if ($label) {
    if($label == 'List price:') {
      $label = 'Regular price:';
    }
    else if($label == 'Price:') {
      $label = 'Your price:';
    }
    
    $output .= '<span class="uc-price-label">' . $label . '</span> ';
  }
  $output .= theme('uc_price', array('price' => $price));
  $output .= drupal_render_children($element);
  $output .= '</div>';

  return $output;
}