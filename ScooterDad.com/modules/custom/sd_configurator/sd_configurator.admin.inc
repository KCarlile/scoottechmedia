<?php
  // $Id$

  /**
  * @file
  * Supporting code for admin functionality.
  *
  */

  function sd_configurator_actions($form, &$form_state) {
    $form = array();
    
    $form['file_example'] = array(
      '#markup'       => 'Download example file: ' .
        l('sd_options_data.xml',
          drupal_get_path('module', 'sd_configurator') . '/sd_options_data.xml'),  
    );
    
    $form['file_upload'] = array(
      '#type'         => 'file',
      '#title'        => t('Parts Options File'),
      '#description'  => t('Upload Parts Options XML file'),
    );
    
    $form['flush_load_parts'] = array(
      '#type'         => 'submit',
      '#value'        => t('Flush & Load Parts Options'),
      '#submit'       => array('sd_configurator_actions_flush_load'),
      '#validate'     => array('sd_configurator_actions_flush_load_validate'),
    );
    
    $form['break_1'] = array(
        '#markup'     => '<hr /><br />',
    );
    
    $form['flush_parts'] = array(
      '#type'         => 'submit',
      '#value'        => t('Flush Parts Options'),
      '#submit'       => array('sd_configurator_actions_flush'),
    );
    
    return $form;
  }
  
  function sd_configurator_actions_flush_load_validate($form, &$form_state) {
    $file = file_save_upload('file_upload', array(
      'file_validate_extensions' => array('xml'),), FALSE, FILE_EXISTS_REPLACE);
    
    if($file) {
      if($file = file_move($file, 'public://' . $file->filename, FILE_EXISTS_REPLACE)) {
        $form_state['values']['file_upload'] = $file;
      } // end if test
      else {
        form_set_error('file', t('Failed to write the uploaded file the site\'s file folder.'));
      } // end else test
    } // end if test
    else {
      form_set_error('file', t('No file was uploaded.'));
    } // end else test
  } // end function sd_configurator_actions_flush_load_validate

    function sd_configurator_actions_flush_load($form, &$form_state) {
    $file = $form_state['values']['file_upload'];
    unset($form_state['values']['file_upload']);
    //$file->status = FILE_STATUS_PERMANENT;
    
    file_save($file);
    drupal_set_message(t('The form has been submitted and the file has been saved, filename: @filename.',
      array('@filename' => $file->filename)));
    
    sd_configurator_flush_nodes();
    sd_configurator_create_nodes($file->uri);
  } // end function sd_configurator_actions_flush_load
  
  function sd_configurator_actions_flush($form, &$form_state) {
    sd_configurator_flush_nodes();
  } // end function sd_configurator_actions_flush
  
  function sd_configurator_flush_nodes() {
    drupal_set_message('Removing Configurator Option nodes');
  
    $sql = 'SELECT nid FROM {node} n WHERE n.type = :type';
    $result = db_query($sql, array(':type' => 'configurator_option'));
    $nids = array();
    foreach($result as $row) {
      $nids[] = $row->nid;
    } // end foreach loop

    // Delete all the nodes at once
    // http://api.drupal.org/api/function/node_delete_multiple/7
    node_delete_multiple($nids);
  } // end function sd_configurator_flush_nodes
  
  function load_data() {
    $xmlData =  drupal_get_path('module', 'sd_configurator') . '/sd_options_data.xml';
  
    if(file_exists($xmlData)) {
      $parts = simplexml_load_file($xmlData);
      //drupal_set_message('<pre>' . htmlspecialchars($parts->asXml()) . '</pre>');

      foreach($parts as $part) {
        drupal_set_message((string) $part->title);
      } // end foreach loop
    } // end if test
    
    return '';
  } // end function load_data
  
  function sd_configurator_create_nodes($xmlData = null) {
    if(!$xmlData) {
      $xmlData =  drupal_get_path('module', 'sd_configurator') . '/sd_options_data.xml';
    } // end if test
    
    if(file_exists($xmlData)) {
      $parts = simplexml_load_file($xmlData);
      //drupal_set_message('<pre>' . htmlspecialchars($parts->asXml()) . '</pre>');

      foreach($parts as $part) {
        drupal_set_message('Creating ' . (string) $part->title);

        $node = new stdClass();
        $node->type = 'configurator_option';
        node_object_prepare($node);

        $node->title    = $part->title;
        $node->language = LANGUAGE_NONE;
        $node->uid      = 1;

        $node->field_part_type[$node->language][0]['value']         = $part->field_part_type;
        $node->field_compat_hic[$node->language][0]['value']        = $part->field_compat_hic;
        $node->field_compat_scssmall[$node->language][0]['value']   = $part->field_compat_scssmall;
        $node->field_compat_scsfull[$node->language][0]['value']    = $part->field_compat_scsfull;
        $node->field_compat_ics[$node->language][0]['value']        = $part->field_compat_ics;
        $node->field_compat_ihc[$node->language][0]['value']        = $part->field_compat_ihc;
        $node->field_compat_tlc[$node->language][0]['value']        = $part->field_compat_tlc;
        $node->field_compat_threaded[$node->language][0]['value']   = $part->field_compat_threaded;
        $node->field_compat_threadless[$node->language][0]['value'] = $part->field_compat_threadless;
        $node->field_compat_alubar[$node->language][0]['value']     = $part->field_compat_alubar;
        $node->field_compat_mgpfork[$node->language][0]['value']    = $part->field_compat_mgpfork;
        $node->field_notes[$node->language][0]['value']             = $part->field_notes;

        $path = 'sd-configurator-options/' . str_replace(' ', '-', strtolower($part->title));
        $node->path = array('alias' => $path);
        $node->status = 1;
        $node->promote = 0;
        $node->sticky = 0;

        if($node = node_submit($node)) {
          node_save($node);
          //drupal_set_message('Node created: NID# ' . $node->nid);
        } // end if test
      } // end foreach loop

    } // end if test
  } // end function sd_configurator_create_nodes