(function ($) {
  $(document).ready(function() {
  	if($("#compatMatrix").length) {
  		/* column coloring */
			setColCompatColor("compat-hic");
			setColCompatColor("compat-scssmall");
			setColCompatColor("compat-scsfull");
			setColCompatColor("compat-ics");
			setColCompatColor("compat-ihc");
			setColCompatColor("compat-tlc");
			setColCompatColor("compat-threaded");
			setColCompatColor("compat-threadless");
			/*setColCompatColor("compat-alubar");*/
			setColCompatColor("compat-mgpfork");

			/* form visibility */
			$("#configForm").hide();

			$("#evalAnother").click(function(event) {
				event.preventDefault();
				$("#configForm").animate({
					height: "toggle",
					opacity: "toggle"
				}, "slow");

				$("#evalAnother").parent().fadeOut();
			});
  	}

		if($("#disclaimerMessage").length) {
			$("#configForm").hide();

			$("#disclaimerAck").click(function() {
				/*
				$("#disclaimerMessage").animate({
					height: "toggle",
					opacity: "toggle"
				}, "slow");

				$("#configForm").animate({
					height: "toggle",
					opacity: "toggle"
				}, "slow");
				*/
				$.get("/scooter-configurator/disclaimer-disack", function() {
					window.location.replace("/scooter-configurator/disclaimer-ack");
				});
			});
		}
	});
})(jQuery);

function setColCompatColor(colName) {
	(function ($) {
		if($("." + colName + " > img.noImg").length < 1) {
  		$("." + colName).each(function() {
				if($(this).find("img").hasClass("yesImg")) {
					$(this).addClass("compatCol");
				}
			});
  	}
	})(jQuery);
}
