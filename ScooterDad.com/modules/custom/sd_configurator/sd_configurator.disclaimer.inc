<?php
  // $Id$

  /**
  * @file
  * Supporting code for disclaimer code.
  *
  */
global $disclaimerHTML;
global $problemHTML;

$disclaimerHTML = <<<EOT
	<div id="disclaimerMessage">
		<h3>Please read the following directions &amp; disclaimer before continuing below.</h3>
		<p>Welcome to the <em>Scooter Configurator</em>! This tool is intended to provide guidance
			with the most difficult steps in determining parts compatibility, namely surrounding
			the headset, bars, fork, and clamp/compression.</p>
		<p>You can specify that you don't know what parts you have or want, but the Scooter
			Configurator can only provide suggestions for one unknown part at a time. If you do
			not specify more than one part, you'll have to make your own determination as to the
			compatibility.</p>
		<p>ScooterDad's <em>Scooter Configurator</em> is a best-effort to provide
			you with information necessary for determining scooter parts compatibility.
			The <em>Scooter Configurator</em> comes with no guarantee of accuracy. You are
			responsible for verifying the compatibility of your parts when making purchases.</p>
		<p>If	you find any errors or if you'd like to provide some missing parts options,
			please contact ScooterDad by email at:
			<a href="mailto:kenny@scooterdad.com">kenny@scooterdad.com</a></p>
		<p><input type="button" id="disclaimerAck"
			value="I acknowledge this disclaimer. Take me to the Scooter Configurator!" /></p>
	</div>
EOT;

$problemHTML = <<<EOT
	<div>
		<p>This information is presented without guarantee of accuracy. You are responsibile
			for ensuring the compatibility of your parts before making any purchases. If you
			find any missing or inaccurate data, please contact ScooterDad at
			<a href="mailto:kenny@scooterdad.com">kenny@scooterdad.com</a></p>
	</div>
EOT;
